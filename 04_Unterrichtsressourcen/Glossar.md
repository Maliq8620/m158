# Glossar

| Wort                          | Bedeutungserklärungen                                        |
| ----------------------------- | ------------------------------------------------------------ |
| Migration                     | Umstellungsprozesse in Datenverarbeitungssystemen            |
| Reengineering                 | Etwas bestehendes neu implementieren (Ähnlich wie Rebuild)   |
| Reverse Engineering           | Das Ableiten oder Nachdokumentieren von Informationen über ein System |
| Rebuild                       | Etwas bestehendes neu bauen (Ähnlich wie Reengineering)      |
| Rehosting / Lift & Shift      | Migration der Kopie einer Anwendung (Oft in die Cloud)       |
| Retire                        | Ersetzen einer bestehenden Anwendung durch eine Cloud-native Applikation |
| Repurchase                    | Wechsel zu einem anderen Produkt, z. B. SaaS                 |
| Retain Software               | Kritische Anwendungen behalten und Migration auf später schieben |
| SaaS                          | Software-as-a-Service                                        |
| DEV (development environment) | Entwicklungsumgebung                                         |
| STAGE (staging environment)   | Testumgebung                                                 |
| LIVE (live environment)       | Im Deutsch auch oft "scharfe" oder Produktion-Umgebung oder  genannt |
| Refactoring                   | Neuaufbau einer Anwendung                                    |
| Platform change               | Migration in eine neue Umgebung                              |
| DevOps                        | DevOps ist eine Kombination aus „Development" und „Operations", steht allerdings für eine ganze Reihe von Ideen und Praktiken, die über diese beiden Begriffe, egal ob einzeln oder zusammen, weit hinausgehen. |
| Legacy App / System           | Abzulösende Anwendung / System                               |
| Interface                     | Schnittstelle                                                |
| Live-Migration                | Umzug einer virtuellen Maschine (VM) im laufenden Betrieb    |
| Database First                | Hierbei wird als erstes das Datenbanksystem auf ein modernes System migriert |
| Database Last                 | Hierbei wird als letztes das Datenbanksystem auf ein modernes System migriert |
| Native App                    | Eine Anwendung, welche für eine bestimmte Geräteplattform geschrieben wurde |
| Web App                       | Eine Anwendung welche im Browser ausg                        |





