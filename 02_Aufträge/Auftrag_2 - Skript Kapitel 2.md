# Auftrag 2 - Skript Kapitel 2

<table>
  <tr>
    <th>Dauer</th>
    <td>30 Minuten</td>
  </tr>
  <tr>
    <th>Arbeit</th>
    <td>Einzelarbeit</td>
  </tr>
     <tr>
    <th>Abgabe</th>
    <td>Im eigenen Repository</td>
  </tr>
</table>

## Beschreibung

Sie erhalten von der Lehrperson das Skript **"Theorie und Strategiewahl bei einer Softwaremigration.pdf"**  in ausgedruckter Form

## Auftrag

1. Lesen Sie die Kapitel 1 & 2 aus dem Skript sorgfältig durch und markieren Sie die Stellen, welche Sie als relevant erachten
2. Erstellen Sie eine neue Datei im Ordner "Abgaben" in Ihrem Repository für das M158 mit dem Namen **"Auftrag 2 - Vorname Nachname.md"**
3. Beantworten Sie anschliessend die folgenden Fragen
   1. Nennen Sie zwei Gründe, wann man eine Software-Migration machen muss
   2. Wie ist die Definition für das Wort Migration im Skript?
   3. Der Begriff Software-Migration gehört zum welchem Bereich des Software-Engineering?
   4. Wie ist die Beziehung zwischen Hard- und Software-Migration?
   5. Was sollte niemals mit einer Software-Migration vermischt werden und warum?
   6. Auf was zielt das Reenginierung bei einer Migration ab?
   7. Was versteht man unter Reverse Enginieering?

4. Pushen Sie das Ergebnis mit der "Comit Message" Auftrag 2"

 

