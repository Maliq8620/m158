# Auftrag 1 - Software-Migrationsbeispiel

<table>
  <tr>
    <th>Dauer</th>
    <td>20 Minuten</td>
  </tr>
  <tr>
    <th>Arbeit</th>
    <td>Einzelarbeit</td>
  </tr>
     <tr>
    <th>Abgabe</th>
    <td>Im eigenen Repository</td>
  </tr>
</table>

## Beschreibung

In diesem **Auftrag 1 - Software-Migrationsbeispiel** sollen Sie ein Beispiel für eine Software-Migration im Detail beschreiben. Überlegen Sie sich zuerst ein Beispiel für eine "imaginäre" Software oder Service-Migration und beantworten Sie anschliessend die folgende Fragen. 

*Am besten nehmen Sie nicht etwas zu einfaches, wie z.B. "Einzelplatz-Migration von MS Office" oder ähnlich.*

### Liste mit Ideen

- Betriebssystemwechsel in einer Umgebung mit mehreren Clients
- Umzug eines Webauftrittes von einem Hosting zum anderen
- Ersetzen des Webauftrittes auf demselben Hosting
- Server-Hardwaremigration ohne Softwareunterbruch
- Migration eines klassischen Serverdienst (DHCP/File/Print)
- Portierung einer lokalen Anwendung in die Cloud
- Eigene Ideen

### Fragen

*Beim beantworten der Fragen dürfen Sie natürlich kreativ sein, es soll aber auch so realistisch wie möglich sein.*

- Wie ist der Titel Ihrer Migration?
- Welche Software oder welcher Service wird migriert?
- Hat die Software technische oder produktive Abhängigkeiten? Wenn ja, welche?
- Was ist der Grund für die Migration?
- Darf es eine "Downtime" der Software geben? Wenn ja, wie lange maximal?
- Welche Kosten entstehen bei einer längeren Downtown als erwartet?



## Auftrag

Erstellen Sie eine neue Datei im Ordner "Abgaben" in Ihrem Repository für das M158 mit dem Namen **"Auftrag 1 - Vorname Nachname.md"**

1. In dieser Datei erstellen Sie einen Titel mit dem Namen Ihrer Software-Migration
2. Beantworten Sie die oben gestellten Fragen in dieser Datei
3. Pushen Sie das Ergebnis mit der "Comit Message" Auftrag 1"

