# Umsetzungsvorschlag - Modul 158

### LB1

Die erste Leistungsbeurteilung ist eine theoretische Prüfung. Die Inhalte für die LB1 stammen aus den theoretischen Aufträgen, den Übungen und dem Glossar.

### LB2

Die zweite Leistungsbeurteilung resultiert aus den praktischen Aufträgen. 


## Lektionenplan

| Tag | Themen                   | Unterlagen / Übungen | Beschreibung | Hausaufgaben |
|---|---------------------------|-------------|-----------------------------------------|-----------------------------------------|
| 1 | - Begriff Migration<br/>- Software und die verschiedenen Varianten<br>- Auftrag 1-3 | - [Software-Migration-Einführung-Klärung-Umfang.pptx](https://gitlab.com/ch-tbz-it/Stud/m158/-/tree/main/04_Unterrichtsressourcen/01_Pr%C3%A4sentationen)<br>- [Berechtigungen_Linux_POSIX.pptx](https://gitlab.com/ch-tbz-it/Stud/m158/-/tree/main/04_Unterrichtsressourcen/01_Pr%C3%A4sentationen) <br>- [Auftrag 1-3](https://gitlab.com/ch-tbz-it/Stud/m158/-/tree/main/02_Auftr%C3%A4ge) | - Am Tag 1 soll der Begriff Migration geklärt werden. <br >- Anschliessend starten die Schülern mit den Aufträgen 1-3. <br/>- Am Schluss gibt es zur Abwechslung eine technische Präsentation über die Berechtigungen unter Linux. | Aufgabe 1 & 2 fertigstellen |
| 2 | - Lösung Auftrag 2<br>- Glossar üben<br>- Präsentation Auftrag 3 <br>- Übung UNC Pfade | - [Übung UNC.pdf](https://gitlab.com/ch-tbz-it/Stud/m158/-/blob/main/04_Unterrichtsressourcen/Übung_UNC.pdf)<br>- [Glossar](https://gitlab.com/ch-tbz-it/Stud/m158/-/blob/main/04_Unterrichtsressourcen/Glossar.md) | - Beginn Repetition (Besprechen der Fragen aus Auftrag 2) <br/>-Glossar zusammen anschauen und kurz besprechen<br>- Weiterarbeiten an Auftrag 3, mit anschliessender Kurzpräsentation der Resultate<br/>- Einführung in das Thema UNC-Pfade mit anschliessendem Übungsblatt | Glossar über für Prüfung |
| 3 |   |  |  |  |
| 4 |   |  |  |  |
| 5 |   |  |  |  |
| 6 |   |  |  |  |
| 7 |   |  |  |  |
| 8 |   |  |  |  |
| 9 |   |  |  |  |
| 10 |   |  |  |  |

---

> [⇧ **Hauptseite**](https://gitlab.com/ch-tbz-it/Stud/m158)
