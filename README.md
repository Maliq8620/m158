![Logo](x_gitressourcen/ZH_logo.jpg)
# M158 - Software-Migration planen und durchführen

## Version der Modulidentifikation

*13.08.2021, V3 ([Modulbaukasten ICT-BCH](https://www.modulbaukasten.ch/module/158/3/de-DE?title=Software-Migration-planen-und-durchf%C3%BChren/)).*

<hr >

## Kurzbeschreibung des Moduls gemäss Modulidentifikation

*Im Modul 158 setzen wir uns mit den Grundlagen der Software-Migration auseinander. Dabei werden wir uns mit der Theorie von Migrationsansätzen und praktischen Übungen unter Linux auseinandersetzen. Ergänzt wird der Unterricht mit Grundlagen, welche passend zum Thema Softwaremigration sind.*





<hr >

## Aufbau der Unterlagen zum Modul

Das Modul 158 besteht aus einem Theorie- sowie einem praktischen Teil. Das Modul ist in folgenden Elementen aufgebaut

- Aufträge
- Präsentationen
- Übungen

#### Aufträge

Aufträge sind selbstständig lösbare Arbeiten, welche die Lernenden ausführen und anschliessend in Ihrem persönlichen Repository ablegen müssen. Die Aufträge sind unterteilt in theoretische und praktische Übungen. Die theoretischen Aufträge sind mit einem (T) und die praktischen mit einem (P) gekennzeichnet. 

- Die theoretischen Aufträge sind für die Theorieprüfung relevant
- Jeder praktische Auftrag gibt max. 3 Punkte welcher für die Berechnung der zweiten Note zählt (Näheres finden dazu unter Kompetenznachweis)

#### Präsentationen

Präsentationen sind Inhalte, welche die lernenden von der Lehrperson im Frontal unterrichtet erhalten. Sie beinhalten Themen, welche für die Aufträge und Übungen relevant sind.

#### Übungen

Übungen dienen den Schülern, ein Grundlagenthema, welches zuvor in der Theorie besprochen wurde, selbstständig zu üben. Die Übungen sind für die Theorieprüfung relevant.



<hr >

### Umsetzungsvorschlag

[Umsetzungsvorschlag](3_Umsetzungsvorschlag) mit möglichem Modulablauf



<hr >

### Unterrichtsressourcen

[Unterrichtsressourcen](2_Unterrichtsressourcen) von Aufträgen und Inhalten zu den einzelnen Kompetenzen



<hr >

### Kompetenznachweise

Der Kompetenznachweis besteht aus zwei Leistungsbeurteilungen LB1 & LB2

#### LB 1

Die erste Leistungsbeurteilung ist eine theoretische Prüfung. Die Inhalte für die LB1 stammen aus den theoretischen Aufträgen, den Übungen und dem Glossar.

#### LB2

Die zweite Leistungsbeurteilung resultiert aus den praktischen Aufträgen. 

